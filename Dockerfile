FROM bscdataclay/client:dev20210615-alpine

# Prepare environment
ENV ELASTIC_MODEL_HOME=/elastic
WORKDIR ${ELASTIC_MODEL_HOME} 

ENV DATACLAYCLIENTCONFIG=${ELASTIC_MODEL_HOME}/cfgfiles/client.properties
ENV DATACLAYGLOBALCONFIG=${ELASTIC_MODEL_HOME}/cfgfiles/global.properties
ENV DATACLAYSESSIONCONFIG=${ELASTIC_MODEL_HOME}/cfgfiles/session.properties
ENV NAMESPACE=ElasticNS
ENV USER=ElasticUser
ENV PASS=ElasticPass
ENV DATASET=ElasticDS
ENV STUBSPATH=${ELASTIC_MODEL_HOME}/stubs
ENV STUBS_JAR=${ELASTIC_MODEL_HOME}/stubs.jar
ENV MODELBINPATH=${ELASTIC_MODEL_HOME}/model/target/classes

# Install maven
RUN apk add --no-cache maven openjdk11-jdk

# If we want to run demo again, argument must be modified 
ARG CACHEBUST=1 

# Copy files 
COPY ./model ${ELASTIC_MODEL_HOME}/model
COPY ./cfgfiles ${ELASTIC_MODEL_HOME}/cfgfiles

# Wait for dataclay to be alive (max retries 10 and 5 seconds per retry)
RUN dataclaycmd WaitForDataClayToBeAlive 10 5

# Register account
RUN dataclaycmd NewAccount ${USER} ${PASS}

# Register datacontract
RUN dataclaycmd NewDataContract ${USER} ${PASS} ${DATASET} ${USER}

# Register model
RUN cd ${ELASTIC_MODEL_HOME}/model && mvn package
RUN dataclaycmd NewModel ${USER} ${PASS} ${NAMESPACE} ${MODELBINPATH} java

# Run 
ENTRYPOINT ["Nothing to do here"] 


