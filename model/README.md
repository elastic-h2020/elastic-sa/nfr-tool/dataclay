# Introduction 

This model is used to communicate the NFR tool with the COMPSs orchestrator. The model defines the Elastic system with its applications and nodes (master and workers). A more detailed view of the model is provided in the next section.


## Model

Registered model has the following representation:

```
                                    +---------------------+
                                    |                     |
                                    |  Master             |
                                    |                     |
+--------------------------+        +---------------------+
|                          |1      1|                     |                    +--------------------------+
|  COMPSsApplication       +<------>+  - int PID          |                    |                          |
|                          |        |  - String IP        |*                  1|  Node                    |
+--------------------------+        |                     +<------------------>+                          |
|                          |        +---------------------+                    +--------------------------+             +---------------------+
|  - String name           |                                                   |                          |             |                     |
|  - String uuid           |     +-------------------------------------+       |  - String ipWifi         |             |  CommunicationLink  |
|  - boolean requiresSecure|1   *|                                     |*     1|  - String ipEth          |*           *|                     |
|  - String infoNature     +<--->+  Worker                             +-------+  - String ipLte          +<----------->+---------------------+
|  - int monitoringPeriod  |     |                                     |       |  - float signalWifi      |             |                     |
|  - float deadlinesMissed |     +-------------------------------------+       |  - float energyThreshold |             |  - String IPNode1   |
|  - boolean isHighPriority|     |  - int PID                          |       |  - float cpuThreshold    |             |  - String IPNode2   |
+--------------------------+     |  - String ip                        |       |  - int numCores          |             |  - float delayRTT   |
          ^*                     |  - boolean active                   |       |  - boolean isSecure      |             |  - float PLR        |
          |                      |  - float cpuUsage                   |       +--------------------------+             |  - float throughput |
          |                      |  - float energyUsage                |                    ^*                          |                     |
          |                      |  - int computingUnits               |                    |                          +---------------------+
          |                      |  - int maxComputingUnits            |                    |                          
          |                      |  - float communicationCost          |                    |
          |                      |  - float deadlinesMissedRatio       |                    |
          |                      |  - List<String> deactivationReasons |                    |
          |                      |  - boolean isContainer              |                    |
          |                      |  - string isContainerName           |                    |
          |                      +-------------------------------------+                    |
          |1                                                                                |
+---------+---------+                                                                       |
|                   |                                                                       |
|  ElasticSystem    |                                                                       |
|                   |                                                                       |
+-------------------+                                                                       |
|                   |1                                                                      |
|                   +<----------------------------------------------------------------------+
+---------+---------+                                                 

```

## Questions? 

If you have any questions, please feel free to ask to support-dataclay@bsc.es

![dataClay logo](https://www.bsc.es/sites/default/files/public/styles/bscw2_-_simple_crop_style/public/bscw2/content/software-app/logo/logo_dataclay_web_bsc.jpg)
