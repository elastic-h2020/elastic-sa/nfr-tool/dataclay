package es.bsc.compss.nfr.model;

import es.bsc.dataclay.DataClayObject;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a worker process.
 */
public class CommunicationLink extends DataClayObject {

	/** Ip of node 1 present in the communication link. */
	private String ipNode1;

	/** Ip of node 2 present in the communication link. */
	private String ipNode2;

	/** Delay in a particular communication link. */
	private float delayRtt;

	/** Packet Loss Rate. */
	private float plr;

	/** Throughput value. */
	private float throughput;

	/** List of all nodes present in a given communication list. */
	private List<Node> nodes = new ArrayList<>();

	/** Boolean value that acts as a dirty cache attribute. If true, delay has been modified by nfrtool, otherwise no
	 * need to update */
	private boolean dirty;


	/**
	 * Constructor.
	 * 
	 * @param node1      Node 1
	 * @param node2      Node 2
	 * @param ipNode1    IP for Node 1
	 * @param ipNode2    IP for Node 2
	 * @param delayRtt   Delay RTT obtained in the communication link
	 * @param plr        Packet loss rate metric obtained in the link
	 * @param throughput Throughput metric obtained in the link
	 */
	public CommunicationLink(final Node node1, final Node node2, final String ipNode1, final String ipNode2,
			final float delayRtt, final float plr, final float throughput) {
		this.ipNode1 = ipNode1;
		this.ipNode2 = ipNode2;
		this.delayRtt = delayRtt;
		this.plr = plr;
		this.throughput = throughput;
		this.nodes.add(node1);
		this.nodes.add(node2);
		dirty = false;
	}

	/**
	 * No-Argument CommunicationLink constructor
	 */
	public CommunicationLink() {
	}

	/**
	 * Get the address of the first node present in the communication link
	 * 
	 * @return string IP of node 1
	 */
	public String getIpNode1() {
		return this.ipNode1;
	}

	/**
	 * Set the address of the first node present in the communication link
	 * 
	 * @param ipNode1 string IP of node 1
	 */
	public void setIpNode1(final String ipNode1) {
		this.ipNode1 = ipNode1;
	}

	/**
	 * Get the address of the second node present in the communication link
	 * 
	 * @return string IP of node 2
	 */
	public String getIpNode2() {
		return this.ipNode2;
	}

	/**
	 * Set the address of the second node present in the communication link
	 * 
	 * @param ipNode2 string IP of node 2
	 */
	public void setIpNode2(final String ipNode2) {
		this.ipNode2 = ipNode2;
	}

	/**
	 * Get the delayRTT present in the communication link between node 1 and node 2
	 * 
	 * @return float delayRtt
	 */
	public float getDelayRtt() {
		return this.delayRtt;
	}

	/**
	 * Set the delayRTT present in the communication link between node 1 and node 2
	 * 
	 * @param delayRtt float
	 */
	public void setDelayRtt(final float delayRtt) {
		this.delayRtt = delayRtt;
		dirty = true;
	}

	/**
	 * Get the packet loss rate present in the communication link between node 1 and
	 * node 2
	 * 
	 * @return float plr
	 */
	public float getPlr() {
		return this.plr;
	}

	/**
	 * Set the PLR present in the communication link between node 1 and node 2
	 * 
	 * @param plr float
	 */
	public void setPlr(final float plr) {
		this.plr = plr;
	}

	/**
	 * Get the throughput value present in the communication link between node 1 and
	 * node 2
	 * 
	 * @return float throughput
	 */
	public float getThroughput() {
		return this.throughput;
	}

	/**
	 * Set the throughput value present in the communication link between node 1 and
	 * node 2
	 * 
	 * @param throughput float
	 */
	public void setThroughput(final float throughput) {
		this.throughput = throughput;
	}

	/**
	 * Checks if delay has been updated by nfr-tool, indicated by dirty value. In any case, dirty ends up being false
	 *
	 * @return boolean result of dirty
	 */
	public boolean hasBeenUpdated() {
		boolean res = dirty;
		dirty = false;
		return res;
	}

	/**
	 * Clears the existing object with the desired default values (for testing and
	 * demo purposes)
	 * 
	 * @param node1      first node object present in the communication link
	 * @param node2      second node object present in the communication link
	 * @param ipNode1    address of the 1st node present in the communication link
	 * @param ipNode2    address of the 2nd node present in the communication link
	 * @param delayRtt   delayRTT in the communication link node 1 -- node 2
	 * @param plr        PLR in the communication link node 1 -- node 2
	 * @param throughput throughput in the communication link node 1 -- node 2
	 */
	public void initValues(Node node1, Node node2, String ipNode1, String ipNode2, float delayRtt, float plr,
			float throughput) {
		this.ipNode1 = ipNode1;
		this.ipNode2 = ipNode2;
		this.delayRtt = delayRtt;
		this.plr = plr;
		this.throughput = throughput;
		this.nodes.add(node1);
		this.nodes.add(node2);
	}

	@Override
	public String toString() {
		return " communication link { IP Node 1 = " + getIpNode1() + " , IP Node 2 = " + getIpNode2() + ", Delay RTT = "
				+ getDelayRtt() + ", PLR = " + getPlr() + ", Throughput = " + getThroughput() + "}";
	}

}
