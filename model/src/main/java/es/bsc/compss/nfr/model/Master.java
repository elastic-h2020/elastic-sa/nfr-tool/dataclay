package es.bsc.compss.nfr.model;

import es.bsc.dataclay.DataClayObject;

/**
 * This class represents a master process.
 */
public class Master extends DataClayObject {

	/** Node containing the process. */
	private Node node;

	/** Master process PID. */
	private int pid;

	/** Master IP. */
	private String ip;

	/** Applications executed by this master. */
	private COMPSsApplication app;

	/**
	 * Constructor.
	 * 
	 * @param node Node containing the process.
	 * @param pid  Master process PID
	 */
	public Master(final Node node, final int pid, final String ip) {
		this.node = node;
		this.pid = pid;
		node.addMaster(this);
		this.ip = ip;
	}

	/**
	 * No-Argument Master constructor
	 */
	public Master() {

	}

	/**
	 * Get node containing Master process
	 * 
	 * @return node containing Master process
	 */
	public Node getNode() {
		return node;
	}

	/**
	 * Get Master's IP
	 * 
	 * @return ip String with the IP of the master node
	 */
	public String getIp() {
		return this.ip;
	}

	/**
	 * Set node containing Master process
	 * 
	 * @param node node containing Master process to set
	 */
	public void setNode(Node node) {
		this.node = node;
	}

	/**
	 * Get PID of Master process
	 * 
	 * @return PID of Master process
	 */
	public int getPid() {
		return pid;
	}

	/**
	 * Set PID of Master process
	 * 
	 * @param pid PID of Master process
	 */
	public void setPid(int pid) {
		this.pid = pid;
	}

	/**
	 * Get application executed by this master
	 * 
	 * @return application executed by the master
	 */
	public COMPSsApplication getApplication() {
		return app;
	}

	/**
	 * Set application executed by the master
	 * 
	 * @param app application launched by the master
	 */
	public void setApplication(final COMPSsApplication app) {
		this.app = app;
	}

	/**
	 * Clears the existing object with the desired default values (for testing and
	 * demo purposes)
	 * 
	 * @param node
	 * @param pid
	 * @param ip
	 */
	public void initValues(Node node, int pid, String ip) {
		this.node = node;
		this.pid = pid;
		node.addMaster(this);
		this.ip = ip;
	}

	@Override
	public String toString() {
		return " master { pid = " + pid + " , node = " + node.toString() + " , application  = " + app.getName() + "}";
	}

}
