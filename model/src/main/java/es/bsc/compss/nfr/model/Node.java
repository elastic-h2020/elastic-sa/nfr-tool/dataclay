package es.bsc.compss.nfr.model;

import es.bsc.dataclay.DataClayObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * This class represents a COMPSs node.
 */
public class Node extends DataClayObject {

	/** Node IP for Wifi Interface. */
	private String ipWifi;

	/** Node IP for Ethernet Interface. */
	private String ipEth;

	/** Node IP for LTE Interface. */
	private String ipLte;

	/** Node CPU threshold. */
	private float cpuThreshold;

	/** Node Energy threshold. */
	private float energyThreshold;

	/** Wifi Signal. */
	private float signalWifi;

	/** Num cores. */
	private int numCores;

	/** List of all masters present in a given node. */
	private List<Master> masters = new ArrayList<>();

	/** List of all workers present in a given node. */
	private List<Worker> workers = new ArrayList<>();

	/** List of all communication links a given node participates in. */
	private List<CommunicationLink> communicationLinks = new ArrayList<>();

	/** Boolean value indicating whether application is secure. */
	private boolean isSecure = true;

	/**
	 * Constructor.
	 * 
	 * @param ipWifi          Node name, which can be either the IP or the hostname
	 * @param ipEth           Node name, which can be either the IP or the hostname
	 * @param ipLte           Node name, which can be either the IP or the hostname
	 * @param cpuThreshold    Threshold of the CPU
	 * @param energyThreshold Energy threshold of the node
	 */
	public Node(final String ipWifi, final String ipEth, final String ipLte, final float cpuThreshold,
			final float energyThreshold, final float signalWifi, final int numCores) {
		this.ipWifi = ipWifi;
		this.ipEth = ipEth;
		this.ipLte = ipLte;
		this.cpuThreshold = cpuThreshold;
		this.energyThreshold = energyThreshold;
		this.signalWifi = signalWifi;
		this.numCores = numCores;
	}

	/**
	 * No-Argument Node constructor
	 */
	public Node() {

	}

	/**
	 * Get node IP for Wifi Interface.
	 * 
	 * @return String ipWifi.
	 */
	public String getIpWifi() {
		return ipWifi;
	}

	/**
	 * Set node IP for Wifi interface.
	 * 
	 * @param ipWifi Node Wifi IP to set.
	 */
	public void setIpWifi(String ipWifi) {
		this.ipWifi = ipWifi;
	}

	/**
	 * Get node IP for Ethernet Interface.
	 * 
	 * @return String ipEth.
	 */
	public String getIpEth() {
		return ipEth;
	}

	/**
	 * Set node IP for Ethernet interface.
	 * 
	 * @param ipEth Node Ethernet IP to set.
	 */
	public void setIpEth(String ipEth) {
		this.ipEth = ipEth;
	}

	/**
	 * Get node IP for LTE Interface.
	 * 
	 * @return String ipLte.
	 */
	public String getIpLte() {
		return ipLte;
	}

	/**
	 * Set node IP for LTE interface.
	 * 
	 * @param ipLTE Node LTE IP to set.
	 */
	public void setIpLte(String ipLTE) {
		this.ipLte = ipLTE;
	}

	/**
	 * Get CPU Threshold of the node
	 * 
	 * @return node cpuThreshold
	 */
	public float getCPUThreshold() {
		return cpuThreshold;
	}

	/**
	 * Set CPU Threshold of the node
	 * 
	 * @param cpuThreshold node threshold to set
	 */
	public void setCPUThreshold(float cpuThreshold) {
		this.cpuThreshold = cpuThreshold;
	}

	/**
	 * Get energy threshold of the node
	 * 
	 * @return float energyThreshold
	 */
	public float getEnergyThreshold() {
		return energyThreshold;
	}

	/**
	 * Set energy Threshold of the node
	 * 
	 * @param energyThreshold node threshold to set
	 */
	public void setEnergyThreshold(float energyThreshold) {
		this.energyThreshold = energyThreshold;
	}

	/**
	 * Get node Wifi signal.
	 * 
	 * @return Node wifi signal.
	 */
	public float getSignalWifi() {
		return signalWifi;
	}

	/**
	 * Set node Wifi signal.
	 * 
	 * @param signalWifi Node wifiSignal to set.
	 */
	public void setSignalWifi(float signalWifi) {
		this.signalWifi = signalWifi;
	}

	/**
	 * Get node number of cores.
	 * 
	 * @return Node number of cores.
	 */
	public int getNumCores() {
		return numCores;
	}

	/**
	 * Set node number of cores.
	 * 
	 * @param numCores Node numCores to set.
	 */
	public void setNumCores(int numCores) {
		this.numCores = numCores;
	}

	/**
	 * Adds a master in the given node in order to be able to traverse the other way
	 * around.
	 * 
	 * @param master Master in the current node.
	 */
	public void addMaster(final Master master) {
		masters.add(master);
	}

	/**
	 * Returns the list of master objects present in a specific node.
	 * 
	 * @return List of master objects
	 */
	public List<Master> getMasters() {
		return masters;
	}

	/**
	 * Remove master from list of registered masters in the node
	 * 
	 * @param master Master object
	 */
	public void removeMaster(Master master) {
		masters.removeIf(m -> m.getNode().getIpWifi() != null && m.getNode().getIpWifi().equals(master.getNode().getIpWifi()));
		masters.removeIf(m -> m.getNode().getIpEth() != null && m.getNode().getIpEth().equals(master.getNode().getIpEth()));
		masters.removeIf(m -> m.getNode().getIpLte() != null && m.getNode().getIpLte().equals(master.getNode().getIpLte()));
	}

	/**
	 * Adds a new worker to the list of workers on a specific node in order to be
	 * able to traverse the classes the other way around.
	 * 
	 * @param
	 */
	public void addWorker(final Worker worker) {
		workers.add(worker);
	}

	/**
	 * Returns the list of worker objects present in a specific node.
	 * 
	 * @return List of workers
	 */
	public List<Worker> getWorkers() {
		return workers;
	}

	/**
	 * Remove worker from list of registered workers in the node
	 * 
	 * @param worker Worker object
	 */
	public void removeWorker(Worker worker) {
		workers.removeIf(w -> w.getNode().getIpWifi() != null && w.getNode().getIpWifi().equals(worker.getNode().getIpWifi()));
		workers.removeIf(w -> w.getNode().getIpEth() != null && w.getNode().getIpEth().equals(worker.getNode().getIpEth()));
		workers.removeIf(w -> w.getNode().getIpLte() != null && w.getNode().getIpLte().equals(worker.getNode().getIpLte()));
	}

	/**
	 * Adds a new communication link to the list of communication links on a
	 * specific node.
	 * 
	 * @param communicationLink object
	 */
	public void addCommunicationLink(final CommunicationLink communicationLink) {
		communicationLinks.add(communicationLink);
	}

	/**
	 * Returns the list of communication links objects present in a specific node.
	 * 
	 * @return List of communicationLinks
	 */
	public List<CommunicationLink> getCommunicationLinks() {
		return communicationLinks;
	}

	/**
	 * Remove a communication link the node was participating in.
	 * 
	 * @param communicationLink CommunicationLink object
	 */
	public void removeCommunicationLink(CommunicationLink communicationLink) {
		communicationLinks.removeIf(c -> c.getIpNode1().equals(communicationLink.getIpNode1())
				&& c.getIpNode2().equals(communicationLink.getIpNode2()));
	}

	/**
	 * 
	 * Clears the existing object with the desired default values (for testing and
	 * demo purposes)
	 * 
	 * @param ipWifi          IP address of the Wifi interface
	 * @param ipEth           IP address of the Ethernet interface
	 * @param ipLte           IP address of the LTE interface
	 * @param cpuThreshold    CPU threshold of the node
	 * @param energyThreshold Energy threshold of the node
	 * @param signalWifi      Node Wifi signal
	 * @param numCores        Node cores number
	 */
	public void initValues(String ipWifi, String ipEth, String ipLte, float cpuThreshold, float energyThreshold,
			float signalWifi, int numCores) {
		this.ipWifi = ipWifi;
		this.ipEth = ipEth;
		this.ipLte = ipLte;
		this.cpuThreshold = cpuThreshold;
		this.energyThreshold = energyThreshold;
		this.signalWifi = signalWifi;
		this.numCores = numCores;
		this.masters = new ArrayList<>();
		this.workers = new ArrayList<>();
		this.communicationLinks = new ArrayList<>();
	}

	/**
	 * Returns the value of the boolean variable showing whether the application is secure.
	 * @return isSecure boolean variable
	 */
	public boolean isSecure() {
		return this.isSecure;
	}

	/**
	 * Setter for the isSecure boolean variable
	 *
	 * @param secure	Boolean variable to set
	 */
	public void setIsSecure(final boolean secure) {
		this.isSecure = secure;
	}

	/**
	 * Checks whether the IP received as parameter is part of any of the IPs of the node.
	 * @param IP String containing the IP to check
	 * @return boolean value indicating whether the IP received is part of the node.
	 */
	public boolean isNodeIP(String IP) {
		if (IP.equals(ipEth) || IP.equals(ipWifi) || IP.equals(ipLte)) {
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return " [ Wifi IP = " + Optional.ofNullable(ipWifi).orElse("null") + " , ETH IP = " +
				Optional.ofNullable(ipEth).orElse("null") + " , LTE IP = " +
				Optional.ofNullable(ipLte).orElse("null") + " , cpuThreshold = "
				+ cpuThreshold + " , energyThreshold = " + energyThreshold + " ]";
	}

}
