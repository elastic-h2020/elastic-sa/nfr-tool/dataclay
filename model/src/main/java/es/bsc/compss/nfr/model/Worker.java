package es.bsc.compss.nfr.model;

import es.bsc.dataclay.DataClayObject;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class represents a worker process.
 */
public class Worker extends DataClayObject {

	/** Node containing the process. */
	private Node node;

	/** Worker process PID. */
	private int pid;

	/** Boolean showing if worker is running applications. */
	private boolean active;

	/** Float containing the current cpu usage of a Worker. */
	private float cpuUsage;

	/** Float containing the current energy usage of a Worker. */
	private float energyUsage;

	/** Number of available computing units of a Worker. */
	private int computingUnits;

	/** Float representing the communication cost of a Worker. */
	private float communicationCost;

	/** Float representing the ratio of deadlines missed for a particular worker. */
	private float deadlinesMissedRatio;

	/** Float representing the availability to receive more work/tasks */
	private float availability;

	/** Worker IP. */
	private String ip;

	/** COMPSsApplication this worker is executing. */
	private COMPSsApplication app;

	/** Maximum number of computing units available at start of execution, set by COMPSs. */
	private int maxComputingUnits;

	/** Boolean representing if the worker is a Docker container (true) or if it is a native host (false). */
	private boolean isContainer;

	/** String representing the container name (id) in case the worker is a Docker container, otherwise empty string. */
	private String containerName;

	/**
	 * Data that represents the reason/s (dimensions) for the deactivation of this
	 * worker
	 */
	private List<String> deactivationReasons;

	/**
	 * Constructor.
	 * 
	 * @param node                Node containing the process
	 * @param pid                 Worker process PID
	 * @param active              Boolean representing if worker is used
	 * @param app                 COMPSs application
	 * @param cpuUsage            Float representing the CPU percentage used
	 * @param energyUsage         Float representing the Energy power used
	 * @param computingUnits      Number of computing units used in the worker
	 * @param ip                  IP of the Worker
	 * @param communicationCost   Float representing the communication cost
	 * @param deactivationReasons Reason/s for the deactivation of this worker
	 */
	public Worker(final Node node, final int pid, final boolean active, final COMPSsApplication app,
			final float cpuUsage, final float energyUsage, final int computingUnits, final String ip,
			final float communicationCost, final List<String> deactivationReasons) {
		this.node = node;
		this.pid = pid;
		this.active = active;
		node.addWorker(this);
		this.app = app;
		this.cpuUsage = cpuUsage;
		this.energyUsage = energyUsage;
		this.computingUnits = computingUnits;
		this.ip = ip;
		this.communicationCost = communicationCost;
		this.deactivationReasons = deactivationReasons;
		this.deadlinesMissedRatio = 0;
		this.availability = 0;
		this.maxComputingUnits = computingUnits;
		this.isContainer = false;
		this.containerName = "";
	}

	/**
	 * No-Argument Worker constructor
	 */
	public Worker() {

	}

	/**
	 * Get node containing worker process
	 * 
	 * @return node containing worker process
	 */
	public Node getNode() {
		return node;
	}

	/**
	 * Set node containing worker process
	 * 
	 * @param node node containing worker process to set
	 */
	public void setNode(Node node) {
		this.node = node;
	}

	/**
	 * Get PID of worker process
	 * 
	 * @return PID of worker process
	 */
	public int getPid() {
		return pid;
	}

	/**
	 * Set PID of worker process
	 * 
	 * @param pid PID of worker process
	 */
	public void setPid(int pid) {
		this.pid = pid;
	}

	/**
	 * Get active value for worker
	 * 
	 * @return boolean active if worker executing apps
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Set active value to the parameter passed to it
	 * 
	 * @param active boolean value: true if executing, false otherwise
	 */
	public void setActive(final boolean active) {
		this.active = active;
	}

	/**
	 * Get current cpu usage of worker
	 * 
	 * @return float cpuUsage
	 */
	public float getCpuUsage() {
		return cpuUsage;
	}

	/**
	 * Set current cpu usage of worker
	 * 
	 * @param cpuUsage float
	 */
	public void setCpuUsage(final float cpuUsage) {
		this.cpuUsage = cpuUsage;
	}

	/**
	 * Get current energy usage of worker
	 * 
	 * @return float energyUsage
	 */
	public float getEnergyUsage() {
		return energyUsage;
	}

	/**
	 * Set current energy usage of worker
	 * 
	 * @param energyUsage float
	 */
	public void setEnergyUsage(final float energyUsage) {
		this.energyUsage = energyUsage;
	}

	/**
	 * Get current deadlines missed ratio of worker
	 * 
	 * @return float deadlinesMissedRatio
	 */
	public float getDeadlinesMissedRatio() {
		return deadlinesMissedRatio;
	}

	/**
	 * Set current deadlines missed ratio of worker
	 * 
	 * @param deadlinesMissedRatio float
	 */
	public void setDeadlinesMissedRatio(final float deadlinesMissedRatio) {
		this.deadlinesMissedRatio = deadlinesMissedRatio;
	}

	/**
	 * Get current number of computing units of worker
	 * 
	 * @return int computingUnits
	 */
	public int getComputingUnits() {
		return computingUnits;
	}

	/**
	 * Set current number of computing units of worker
	 * 
	 * @param computingUnits int
	 */
	public void setComputingUnits(final int computingUnits) {
		this.computingUnits = computingUnits;
	}

	/**
	 * Get IP of worker
	 * 
	 * @return String ip
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * Set IP of worker
	 * 
	 * @param ip String
	 */
	public void setIp(final String ip) {
		this.ip = ip;
	}

	/**
	 * Get communication cost of worker
	 * 
	 * @return float communicationCost
	 */
	public float getCommunicationCost() {
		return communicationCost;
	}

	/**
	 * Set communication cost of worker
	 * 
	 * @param communicationCost float
	 */
	public void setCommunicationCost(final float communicationCost) {
		this.communicationCost = communicationCost;
	}

	/**
	 * Get availability of worker
	 * 
	 * @return float availability
	 */
	public float getAvailability() {
		return availability;
	}

	/**
	 * Set availability of worker
	 * 
	 * @param availability float
	 */
	public void setAvailability(final float availability) {
		this.availability = availability;
	}

	/**
	 * Get application that worker is executing
	 * 
	 * @return list of all applications
	 */
	public COMPSsApplication getApplication() {
		return app;
	}

	/**
	 * Sets an application to the worker
	 * 
	 * @param app new application that this worker is executing
	 */
	public void setApplication(final COMPSsApplication app) {
		this.app = app;
	}

	/**
	 * Get communications links that worker has in a specific application
	 * 
	 * @return list of all communications links between this worker and others in a
	 *         given application
	 */
	public List<CommunicationLink> getCommunicationLinksForApplication() {
		return node.getCommunicationLinks().stream()
				.filter(c -> c.getIpNode1().equals(this.ip) && app.getWorkers().stream()
						.filter(w -> !w.getIp().equals(this.ip)).anyMatch(w -> w.getIp().equals(c.getIpNode2())))
				.collect(Collectors.toList());
	}

	/**
	 * Get the reason/s for the deactivation of this worker
	 * 
	 * @return dimensions (time, energy, communication or security)
	 */
	public List<String> getDeactivationReasons() {
		return deactivationReasons;
	}

	/**
	 * Set the reason/s for the deactivation of this worker
	 * 
	 * @param deactivationReason new reason/s for the deactivation of this worker
	 */
	public void setDeactivationReasons(final List<String> deactivationReasons) {
		this.deactivationReasons = deactivationReasons;
	}

	/**
	 * Clears the existing object with the desired default values (for testing and
	 * demo purposes)
	 * 
	 * @param node
	 * @param pid
	 * @param active
	 * @param app
	 * @param cpuUsage
	 * @param energyUsage
	 * @param computingUnits
	 * @param ip
	 * @param communicationCost
	 * @param deactivationReasons
	 */
	public void initValues(Node node, int pid, boolean active, COMPSsApplication app, float cpuUsage, float energyUsage,
			int computingUnits, String ip, float communicationCost, List<String> deactivationReasons) {
		this.node = node;
		this.pid = pid;
		this.active = true;
		node.addWorker(this);
		this.app = app;
		this.cpuUsage = cpuUsage;
		this.energyUsage = energyUsage;
		this.computingUnits = computingUnits;
		this.ip = ip;
		this.communicationCost = communicationCost;
		this.deactivationReasons = deactivationReasons;
		this.deadlinesMissedRatio = 0;
		this.availability = 0;
	}

	/**
	 * Sets the container name of the worker to containerName
	 * @param containerName
	 */
	public void setContainerName(final String containerName) {
		this.containerName = containerName;
	}

	/**
	 * Returns the containerName of the worker. The return value can be either a non-empty String (in case the worker is
	 * a Docker container, isContainer == true), or an empty one (in case the worker is a native one,
	 * isContainer == false)
	 * @return containerName
	 */
	public String getContainerName() {
		return containerName;
	}

	/**
	 * COMPSs sets the value of isCointaner to true if a Docker container, or to false if a native one.
	 * @param isContainer
	 */
	public void setIsContainer(final boolean isContainer) {
		this.isContainer = isContainer;
	}

	/**
	 * Returns the value of isContainer
	 * @return isContainer
	 */
	public boolean getIsContainer() {
		return isContainer;
	}

	/**
	 * Sets the maximum number of Computing Units to the initial value deployed by COMPSs.
	 * @param maxCpus
	 */
	public void setMaxComputingUnits(final int maxCpus) {
		maxComputingUnits = maxCpus;
	}

	/**
	 * Returns the maximum number of computing units deployed initially by COMPSs.
	 * @return maxComputingUnits
	 */
	public int getMaxComputingUnits() {
		return maxComputingUnits;
	}

	@Override
	public String toString() {
		return " worker { pid = " + pid + " , address = " + getIp() + ", node = " + node.toString() + ", application = "
				+ app.getName() + ", isContainer = " + isContainer + ", maxComputingUnits = " + maxComputingUnits + "}";
	}

}
