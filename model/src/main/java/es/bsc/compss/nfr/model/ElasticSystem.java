package es.bsc.compss.nfr.model;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/** This class simulates an ELASTIC system composed of many applications and nodes using COMPSs. Objects instantiated by this class can be
 * distributed and synchronized among nodes. */
public class ElasticSystem {

	/** List of nodes in the system. */
	private ArrayList<Node> nodes;
	
	/** Applications in the system. */
	private ArrayList<COMPSsApplication> apps;

	/** Boolean value indicating whether priority is supported. */
	private boolean prioritySupported = false;

	/**
	 * System constructor empty.
	 *
	 */
	public ElasticSystem() {
		apps = new ArrayList<>();
		nodes = new ArrayList<>();
	}
	
	/**
	 * System constructor with already defined apps and nodes.
	 * @param apps List of applications
	 * @param nodes List of nodes
	 */
	public ElasticSystem(final ArrayList<COMPSsApplication> apps, final ArrayList<Node> nodes) {
		this.apps = new ArrayList<>(apps);
		this.nodes = new ArrayList<>(nodes);
	}

	
	/** 
	 * Add a new node to the system. 
	 * @param node Node to be added
	 */
	public void addNode(final Node node) {
		nodes.add(node);
	}

	/**
	 * Add a new application to the system.
	 * @param app COMPSsApplication to be added
	 */
	public void addApplication(final COMPSsApplication app)  {
		apps.add(app);
	}

    /**
     * Remove an existing application. This must be run when the app's execution finishes.
     * @param app COMPSsApplication to be removed
     * @return true if the list contained the specified application
     */
	public boolean removeApplication(final COMPSsApplication app) {
	    return apps.remove(app);
    }
	
	/**
	 * Get system nodes.
	 * @return System nodes 
	 */
	public ArrayList<Node> getNodes()  {
		return this.nodes;
	}
	
	/**
	 * Get applications in the system
	 * @return apps 
	 */
	public ArrayList<COMPSsApplication> getApplications() {
		return this.apps;
	}


	/** WIP
	 * Remove all nodes that have been ONLY deployed by this application. If there is any node that have any other
	 * worker/master from another COMPSs execution, the node will not be removed
	 */
	public void removeDeployedNodes(ArrayList<Node> nodesToRemove, final COMPSsApplication app) {
		// try to remove the Node containing the master
		System.out.println("Nodes to be removed are:");
		for (Node n : nodesToRemove)
			System.out.println("Node " + n);
		nodes.removeAll(nodesToRemove);
		// also remove objects from node
		// TODO: do all operation done in NFRIntegration shutdown here
		app.removeDeployedNodes();
	}

	public ArrayList<Node> getDeployedNodesToRemove(final COMPSsApplication app) {
		return app.getNodesToRemove();
	}

	/**
	 * Returns whether the system has support for priorities.
	 * @return prioritySupported
	 * 
	 */
	public boolean prioritySupported() {
		return prioritySupported;
	}

	/**
	 * Set the value of prioritySupported based on the provided priority boolean.
	 * @param priority
	 */
	public void setPrioritySupported(boolean priority) {
		prioritySupported = priority;
	}

	/**
	 *
	 * @param IP IP to select Node
	 * @return Node node object with IP matching IP
	 */
	public Node IPtoNode(String IP) {
		Node node = null;
		for (Node n : nodes) {
			if (n.isNodeIP(IP)) {
				node = n;
			}
		}
		return node;
	}

	
	@Override
	public String toString() { 
		String result = "Applications: " + apps.stream().map(COMPSsApplication::getName).collect(Collectors.toList()) + " \n";
		result += "   - nodes: " + nodes.stream().map(Node::getIpWifi).collect(Collectors.toList()) + "\n";
		// TODO: check only
		result += "   - nodes (print IPs that are not null): " + nodes.stream().map(n -> Optional.ofNullable(n.getIpWifi()).orElse(Optional.ofNullable(n.getIpEth()).orElse(n.getIpLte()))).collect(Collectors.toList()) + "\n";
		return result;
	}
}
