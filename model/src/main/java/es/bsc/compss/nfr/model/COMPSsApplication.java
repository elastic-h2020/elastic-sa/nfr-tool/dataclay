package es.bsc.compss.nfr.model;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class simulates an ELASTIC application using COMPSs. Objects
 * instantiated by this class can be distributed and synchronized among nodes.
 */
public class COMPSsApplication {

	/** COMPSsApplication name. */
	private String name;

	/**
	 * COMPSsApplication UUID (to be able to tell it apart of other workflows with
	 * the same name)
	 */
	private String uuid;

	/** Application security attribute. */
	private boolean requiresSecure = true;

	/** Application's information nature (E.x: Video stream, location data). */
	private String infoNature;

	/** Application monitoring period attribute. */
	private int monitoringPeriod;

	/** Application deadlines missed ratio. */
	private float deadlinesMissed;

	/** COMPSsApplication master process. */
	private Master master;

	/** COMPSsApplication workers. */
	private List<Worker> workers = new ArrayList<>();

	/** Application priority attribute. */
	private boolean isHighPriority = false;

	/**
	 * COMPSsApplication constructor.
	 * 
	 * @param appName Name of the application
	 */
	public COMPSsApplication(final String appName, final String uuid, final int monitoringPeriod) {
		this.name = appName;
		this.uuid = uuid;
		this.monitoringPeriod = monitoringPeriod;
		deadlinesMissed = 0;
	}

	/**
	 * No-Argument COMPSsApplication constructor
	 */
	public COMPSsApplication() {
	}

	/**
	 * Get application master.
	 * 
	 * @return COMPSsApplication master
	 */
	public Master getMaster() {
		return this.master;
	}

	/**
	 * Set application master.
	 * 
	 * @param themaster COMPSsApplication's master node
	 */
	public void setMaster(final Master themaster) {
		this.master = themaster;
	}

	/**
	 * Add a new woker to the application.
	 * 
	 * @param worker Worker to be added
	 */
	public void addWorker(final Worker worker) {
		workers.add(worker);
	}

	/**
	 * Get application workers.
	 * 
	 * @return COMPSsApplication workers
	 */
	public List<Worker> getWorkers() {
		return this.workers;
	}

	/**
	 * Get active workers only.
	 * 
	 * @return COMPSsApplication active workers
	 */
	public List<Worker> getActiveWorkers() {
		return workers.stream().filter(Worker::isActive).collect(Collectors.toList());
	}

	/**
	 * Get application name
	 * 
	 * @return name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Get application security status
	 * 
	 * @return requiresSecure
	 */
	public boolean requiresSecure() {
		return this.requiresSecure;
	}

	/**
	 * Get application information status
	 * 
	 * @return infoNature 
	 */
	public String getInfoNature() {
		return this.infoNature;
	}

	/**
	 * Set application's information nature.
	 * 
	 * @param infoNature Application's information nature
	 */
	public void setInfoNature(String infoNature) {
		this.infoNature = infoNature;
	}

	/**
	 * Get application UUID
	 * 
	 * @return uuid
	 */
	public String getUuid() {
		return this.uuid;
	}

	/**
	 * Get application monitoring period attribute
	 * 
	 * @return monitoringPeriod
	 */
	public int getMonitoringPeriod() {
		return this.monitoringPeriod;
	}

	/**
	 * Set application's monitoring period
	 * 
	 * @param monitoringPeriod Application's monitoring period
	 */
	public void setMonitoringPeriod(int monitoringPeriod) {
		this.monitoringPeriod = monitoringPeriod;
	}

	/**
	 * Get application deadlines missed ratio attribute
	 * 
	 * @return deadlinesMissed
	 */
	public float getDeadlinesMissedRatio() {
		return this.deadlinesMissed;
	}

	/**
	 * Set application's deadlines missed ratio
	 * 
	 * @param deadlinesMissed Application's deadlines missed ratio
	 */
	public void setDeadlinesMissedRatio(float deadlinesMissed) {
		this.deadlinesMissed = deadlinesMissed;
	}

	/**
	 * Remove all workers from application
	 */
	public void removeWorkers() {
		this.workers.removeAll(this.workers);
	}



	/** WIP
	 * Remove all nodes that have been ONLY deployed by this application. If there is any node that have any other
	 * worker/master from another COMPSs execution, the node will not be removed
	 *
	 * @return Returns the list of Nodes that can be removed from ElasticSystem
	 */
	public void removeDeployedNodes() {
		// set to null so Garbage collector can act upon them
		master = null;
		workers.clear();
	}

	/** TODO: change and update both this and below description with according changes. ALSO CHECK IF NODE IS ALREADY
	 * TODO: IN ArrayList -> Map or set <name, node> and convert it to ArrayList to avoid duplicates
	 * Remove all nodes that have been ONLY deployed by this application. If there is any node that have any other
	 * worker/master from another COMPSs execution, the node will not be removed
	 *
	 * @return Returns the list of Nodes that can be removed from ElasticSystem
	 */
	public ArrayList<Node> getNodesToRemove() {
		// find the Node containing the master and check if can be removed, that is, no other master/workers from other
		// COMPSs executions are hosted there
		ArrayList<Node> toRemove = new ArrayList<>();
		Node node = master.getNode();
		if (canNodeBeRemoved(node))
			toRemove.add(node);

		// get all Nodes hosting the workers that can be removed
		for (Worker w : workers) {
			node = w.getNode();
			if (canNodeBeRemoved(node))
				toRemove.add(node);
		}
		return toRemove;
	}

	// TODO: If check with this is not correct, check by name + uuid
	private boolean canNodeBeRemoved(Node node) {
		System.out.println("NODE " + node + " trying to be removed. First condition is " +
				node.getWorkers().stream().noneMatch(w -> w.getApplication() != this) + " and second condition is " +
				node.getMasters().stream().noneMatch(m -> m.getApplication() != this));
		return (node.getWorkers().stream().noneMatch(w -> w.getApplication() != this) &&
				node.getMasters().stream().noneMatch(m -> m.getApplication() != this));
	}



	/**
	 * Clears the existing object with the desired default values (for testing and
	 * demo purposes)
	 * 
	 * @param appName          application name
	 * @param uuid             application uuid
	 * @param monitoringPeriod application monitoring period
	 * @param infoNature       application information status
	 */
	public void initValues(String appName, String uuid, int monitoringPeriod, String infoNature) {
		this.name = appName;
		this.uuid = uuid;
		this.requiresSecure = true;
		this.infoNature = infoNature;
		this.monitoringPeriod = monitoringPeriod;
		this.deadlinesMissed = 0;
		this.master = new Master();
		this.workers = new ArrayList<>();
	}

	/**
	 * Returns whether the application is high priority or not
	 *
	 * @return isHighPriority
	 */
	public boolean isHighPriority() {
		return isHighPriority;
	}

	public void setHighPriority(boolean hp) {
		isHighPriority = hp;
	}

	@Override
	public String toString() {
		return "COMPSsApplication: " + name + " \n" 
				+ "   - master: " + master.toString() + "\n" 
				+ "   - workers: " + workers + "\n" 
				+ "   - active workers: " + getActiveWorkers() + "\n" 
				+ "   - info nature: " + getInfoNature() + "\n" 
				+ "   - is secure: " + Boolean.toString(requiresSecure()) + "\n"
				+ "   - is high priority: " + Boolean.toString(isHighPriority());
	}
}
