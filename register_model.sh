#!/bin/bash
SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
grn=$'\e[1;32m'
blu=$'\e[1;34m'
red=$'\e[1;91m'
end=$'\e[0m'
function printError { 
  echo "${red}======== $1 ========${end}"
}
function printMsg { 
  echo "${blu}======== $1 ========${end}"
}

printMsg "Registering model"
docker build --network=dataclay_default \
	--build-arg CACHEBUST=$(date +%s) \
	-t dataclay-elastic-model .			
	 
printMsg "Model successfully registered!"
    