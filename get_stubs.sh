#!/bin/bash
SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
grn=$'\e[1;32m'
blu=$'\e[1;34m'
red=$'\e[1;91m'
end=$'\e[0m'
function printError { 
  echo "${red}======== $1 ========${end}"
}
function printMsg { 
  echo "${blu}======== $1 ========${end}"
}

printMsg "Get stubs"

USER=ElasticUser
PASS=ElasticPass
NAMESPACE=ElasticNS
STUBSPATH=/elastic/stubs
docker run --rm --network=dataclay_default \
   -v $PWD/cfgfiles/:/home/dataclayusr/dataclay/cfgfiles/:ro \
   -v $PWD/stubs:/elastic/stubs:rw \
	 bscdataclay/client:dev20210615-alpine GetStubs ${USER} ${PASS} ${NAMESPACE} ${STUBSPATH}

echo "$blu 
Stubs stored at $PWD/stubs/ directory
Congratulations! Now you can use stubs folder in your application CLASSPATH or create a jar. Also
use $PWD/cfgfiles/session.properties in your application. 
Remember to modify StubsClasspath variable in session.properties file. $end

"
    
