# ELASTIC dataClay

This repository contains docker service definition to deploy dataClay and ELASTIC model to be registered 
in dataClay. 

You can find dataClay demos at: 

https://github.com/bsc-dom/dataclay-demos

## Scripts

Some useful scripts can be found here. 

##### START DATACLAY (`start_dataclay.sh`)

Start dataClay before running our demo docker application. 

##### REGISTER MODEL (`register_model.sh`)

Register model located in `model` folder into started dataClay instance at host and ports specified in `cfgfiles/client.properties`. This script uses `Dockerfile` to register the model.

##### GET STUBS (`get_stubs.sh`)

Get stubs of your registered model. Make sure you registered your model first.  

##### STOP DATACLAY (`stop_dataclay.sh`)

Do a graceful stop of dataClay. 

##### OPTIONAL: CLEAN UP (`clean.sh`)

Make sure no dataClay docker services are running and clean volumes.

## Questions? 

If you have any questions, please feel free to ask to support-dataclay@bsc.es

![dataClay logo](https://www.bsc.es/sites/default/files/public/styles/bscw2_-_simple_crop_style/public/bscw2/content/software-app/logo/logo_dataclay_web_bsc.jpg)

## Acknowledgements
This work has been supported by the EU H2020 project ELASTIC, contract #825473.
