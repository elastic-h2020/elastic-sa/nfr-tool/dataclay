#!/bin/bash
SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
grn=$'\e[1;32m'
blu=$'\e[1;34m'
red=$'\e[1;91m'
end=$'\e[0m'
function printError { 
  echo "${red}======== $1 ========${end}"
}
function printMsg { 
  echo "${blu}======== $1 ========${end}"
}
printMsg "Stopping dataClay"
pushd $SCRIPTDIR/dataclay
STARTTIME=$(date +%s)
docker-compose down
ENDTIME=$(date +%s)
echo "dataClay stopped in $(($ENDTIME - $STARTTIME)) seconds"
popd
printMsg "dataClay successfully stopped!"
    
